cp -u ~/.config/i3/config ./i3/config
cp -u ~/.config/i3/polybar.sh ./i3/polybar.sh
cp -u ~/.xinitrc ./.xinitrc
cp -u ~/.gitconfig ./.gitconfig
cp -u ~/.zshrc ./.zshrc
cp -u ~/.zprofile ./.zprofile
cp -u ~/.zpreztorc ./.zpreztorc
cp -u ~/.tmux.conf ./.tmux.conf
cp -u ~/.ctags ./.ctags
cp -u ~/.config/polybar/config ./polybar/config
cp -u ~/.config/compton.conf ./compton.conf
cp -u ~/.config/nvim/init.vim ./nvim/init.vim
cp -u ~/.config/nvim/coc-settings.json ./nvim/coc-settings.json
cp -u ~/.config/nvim/autoload/plug.vim ./nvim/autoload/plug.vim
cp -u ~/.config/nvim/conf/* ./nvim/conf/
cp -u ~/.config/nvim/neosnippets/* ./nvim/neosnippets/
cp -u ~/.config/coc/ultisnips/* ./coc/ultisnips/
cp -u ~/.config/ranger/rc.conf ./ranger/rc.conf
cp -u ~/.config/ranger/rifle.conf ./ranger/rifle.conf
# cp -u ~/.config/vifm/vifmrc ~/Documents/Dotfiles/vifm
# cp -u ~/.config/mpd/* ./mpd/
