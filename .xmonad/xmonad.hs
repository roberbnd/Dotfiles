import qualified Data.Map as M
import XMonad
import XMonad.Util.Run(spawnPipe)
import qualified XMonad as XMonad
import XMonad.Hooks.SetWMName
import qualified XMonad.StackSet as W

myWorkspaces = map show [1..9]
myNumRow = [xK_ampersand
           , xK_bracketleft
           , xK_braceleft
           , xK_braceright
           , xK_parenleft
           , xK_equal
           , xK_asterisk
           , xK_parenright
           , xK_plus] 

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@(XConfig {modMask = modm}) =
  M.fromList $ [((m.|. modm, k), windows $ f i)
             | (i,k) <- zip (XMonad.workspaces conf) myNumRow
             , (f,m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

-- Matlab Java gui workaround.
myStartupHook = setWMName "LG3D" 

------------------------------------------------------------------------------
main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ defaultConfig 
        { terminal = "tilix"
        , keys    = \c -> myKeys c `M.union` (keys defaultConfig c)
        , startupHook = myStartupHook
        }
-- https://gist.github.com/imalsogreg/5c8b431c3f5626c4e533
