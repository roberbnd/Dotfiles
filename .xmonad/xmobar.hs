-- xmobar config used by Vic Fryzel
-- Author: Vic Fryzel
-- http://github.com/vicfryzel/xmonad-config

-- This is setup for dual 1920x1080 monitors, with the right monitor as primary
Config {
        additionalFonts = []
        borderColor = "black"
        border = TopB
        bgColor = "black"
        fgColor = "grey"
        alpha = 255
        position = Top
        textOffset = -1
        iconOffset = -1
        lowerOnStart = True
        pickBroadest = False
        persistent = False
        hideOnStart = False
        iconRoot = "."
        allDesktops = True
        overrideRedirect = True
        font = "xft:Roboto:size=10:bold"
        commands = [
            Run Date "%a %b %_d %l:%M" "date" 10,
            -- Run Com "getMasterVolume" [] "volumelevel" 10,
            Run StdinReader
                   ],
        sepChar = "%",
        alignSep = "}{",
        template = "%StdinReader% }{  |  <fc=#FFFFCC>%date%</fc>"
        }
