call plug#begin('~/.config/nvim/plugged')

source ~/.config/nvim/conf/ui.vim
source ~/.config/nvim/conf/base.vim
source ~/.config/nvim/conf/organization.vim
source ~/.config/nvim/conf/codeOrganization.vim
source ~/.config/nvim/conf/refactoring.vim
source ~/.config/nvim/conf/tmux.vim
source ~/.config/nvim/conf/languages.vim
source ~/.config/nvim/conf/javascript.vim
source ~/.config/nvim/conf/python.vim
source ~/.config/nvim/conf/rust.vim
source ~/.config/nvim/conf/ruby.vim
source ~/.config/nvim/conf/elixir.vim
source ~/.config/nvim/conf/go.vim
source ~/.config/nvim/conf/haskell.vim
source ~/.config/nvim/conf/html.vim
source ~/.config/nvim/conf/css.vim
source ~/.config/nvim/conf/git.vim
source ~/.config/nvim/conf/objects.vim
source ~/.config/nvim/conf/util.vim

call plug#end()

source ~/.config/nvim/conf/set.vim
source ~/.config/nvim/conf/scripts.vim
source ~/.config/nvim/conf/keyboard.vim
source ~/.config/nvim/conf/autocmd.vim
