"emmet{{{=================================================================================
" <c-z>, by configuration
Plug 'mattn/emmet-vim'
" Emmet
let g:user_emmet_leader_key='<c-e>'
"======================================================================================}}}

"tagalong{{{==============================================================================
" The plugin is designed to automatically
" rename closing HTML/XML tags when editing
" opening ones (or the other way around).
Plug 'AndrewRadev/tagalong.vim'
let g:tagalong_mappings = ['c', 'C']
let g:tagalong_additional_filetypes = ['javascript']
let g:tagalong_filetypes = ['html', 'xml', 'jsx', 'eruby', 'ejs', 'eco', 'php', 'htmldjango', 'js']
"======================================================================================}}}
