"vim-argwrap{{{===========================================================================
" ArgWrap is an industrial strength argument wrapping and unwrapping extension for the Vim
" text editor.  It can be used for collapsing and expanding everything
" from function calls to array and dictionary definitions.  All operations are easily
" reversible and correctly preserve the indentation of the surrounding code.
" :ArgWrap
Plug 'foosoft/vim-argwrap', { 'on': 'ArgWrap' }
"======================================================================================}}}

"splitjoin{{{=============================================================================
" switching between a single-line statement and a multi-line one.
" It offers the following default keybindings, which can be customized:
" gS to split a one-liner into multiple lines
" gJ (with the cursor on the first line of a block)
"     to join a block into a single-line statement.
"     I usually work with ruby and a lot of expressions
"     can be written very concisely on a single line.
"  A good example is the "if" statement:
" puts "foo" if bar?
" This is a great feature of the language, but when you
" need to add more statements to the body of the "if", you need to rewrite it:
" if bar?
"   puts "foo"
"   puts "baz"
" end
" The idea of this plugin is to introduce a single key binding
" (default: gS) for transforming a line like this:
" <div id="foo">bar</div>
" Into this:
" <div id="foo">
"   bar
" </div>
Plug 'AndrewRadev/splitjoin.vim'
"======================================================================================}}}

"sideways{{{==============================================================================
" The plugin defines two commands, :SidewaysLeft
" and :SidewaysRight, which move the item under
" the cursor left or right, where an "item"
" is defined by a delimiter
Plug 'AndrewRadev/sideways.vim'
"======================================================================================}}}

"breakit{{{===============================================================================
" <Leader>ba, - break after comma
" <Leader>bA, - break after comma remove space
" <Leader>bb, - break before comma
" <Leader>ba. - break after dot
" <Leader>bA. - break after dot remove space
" <Leader>bb. - break before dot
" test, test2, test3
" <Leader>ba,
" test,
"  test2,
"  test3
Plug 'bomgar/breakit.vim'
"======================================================================================}}}

"tabular{{{===============================================================================
" :Tabularize /{pattern}
Plug 'godlygeek/tabular', { 'on': 'Tabularize' }
"======================================================================================}}}

"vim-easy-align{{{========================================================================
" A simple, easy-to-use Vim alignment plugin.
Plug 'junegunn/vim-easy-align'
"======================================================================================}}}

"vim-exchange{{{==========================================================================
" To exchange two words, place your cursor on the first word and type cxiw. Then move
" to the second word and type cxiw again. Note: the {motion} used in the first
" and second use of cx don't have to be the same.
" nmap cx <Plug>(Exchange)
" vmap X <Plug>(Exchange)
" nmap cxc <Plug>(ExchangeClear)
" nmap cxx <Plug>(ExchangeLine)
Plug 'tommcdo/vim-exchange'
"======================================================================================}}}
