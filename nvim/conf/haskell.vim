"haskell-vim{{{===========================================================================
" Syntax Highlighting and Indentation for Haskell and Cabal
Plug 'neovimhaskell/haskell-vim', { 'for': 'haskell' }
"======================================================================================}}}

"idris-vim{{{=============================================================================
" This is an Idris mode for vim which features
" syntax highlighting, indentation and optional
" syntax checking via Syntastic. If you need a
" REPL I recommend using Vimshell.
Plug 'idris-hackers/idris-vim', { 'for': 'idris' }
"======================================================================================}}}

"vim-scala{{{=============================================================================
" This is a "bundle" for Vim that builds off
" of the initial Scala plugin modules by
" Stefan Matthias Aust and adds some more
" "stuff" that I find useful, including all
" of my notes and customizations.
" Sorting of import statements
" :SortScalaImports
" There are different modes for import sorting available.
" :help :SortScalaImports
Plug 'derekwyatt/vim-scala', { 'for': 'scala' }
"======================================================================================}}}
