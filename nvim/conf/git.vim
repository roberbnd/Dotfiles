"git-messenger{{{=========================================================================
" git-messenger.vim is a Vim/Neovim plugin to
" reveal the hidden message from Git under the
" cursor quickly. It shows the history of commits
" under the cursor in popup window.
" This plugin shows the message of the last commit
" in a 'popup window'. If the last commit is not
" convenient, you can explore older commits in the
" popup window. Additionally you can also check
" diff of the commit.
Plug 'rhysd/git-messenger.vim', { 'on': 'GitMessenger' }
"======================================================================================}}}

"vim-gitgutter{{{=========================================================================
" A Vim plugin which shows a git diff in the 'gutter' (sign column).
" It shows whether each line has been added, modified,
" and where lines have been removed.
" You can also stage and undo individual hunks.
Plug 'airblade/vim-gitgutter'
"======================================================================================}}}

"vim-fugitive{{{==========================================================================
Plug 'tpope/vim-fugitive'
"======================================================================================}}}
