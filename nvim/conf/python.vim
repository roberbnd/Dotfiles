"semshi{{{================================================================================
" Semantic Highlighting for Python in Neovim
Plug 'numirias/semshi', { 'for': 'python', 'do': ':UpdateRemotePlugins'}
"======================================================================================}}}

"vim-python-pep8-indent{{{================================================================
" This small script modifies Vim’s indentation
" behavior to comply with PEP8 and my aesthetic
" preferences.
Plug 'Vimjas/vim-python-pep8-indent', { 'for': 'python'}
"======================================================================================}}}

"vim-insort{{{============================================================================
" Just call the :Isort command, and it will reorder
" the imports of the current python file.
" Or select a block of imports with visual mode,
" and press Ctrl-i to sort them.
" pip install isort
Plug 'fisadev/vim-isort', { 'for': 'python' }
"======================================================================================}}}

"django{{{================================================================================
Plug 'vim-scripts/django.vim', { 'for': 'python' }
"======================================================================================}}}

"vim-coiled-snake{{{======================================================================
Plug 'kalekundert/vim-coiled-snake', { 'for': 'python' }
"======================================================================================}}}
