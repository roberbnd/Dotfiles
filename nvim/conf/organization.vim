"vim-misc{{{==============================================================================
"Needed for vim-session and vim-notes
Plug 'xolox/vim-misc'
"======================================================================================}}}

"vim-session{{{===========================================================================
" It works by generating a Vim script that restores your
" current settings and the arrangement of tab pages
" and/or split windows and the files they contain.
Plug 'xolox/vim-session'

let g:session_autosave = 'no'
let g:session_directory = '~/.config/nvim/sessions'
"======================================================================================}}}

"vim-notes{{{=============================================================================
" The vim-notes plug-in for the [Vim text editor]
" vim makes it easy to manage your notes in Vim:
Plug 'xolox/vim-notes'

let g:notes_suffix = '.md'
"======================================================================================}}}
