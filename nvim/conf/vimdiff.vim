"vim-diffchar{{{==========================================================================
" This plugin has been developed in order to make diff mode more useful. Vim highlights all the
" text in between the first and last different characters on a changed line. But this plugin
" will find the exact differences between them, character by character - so called DiffChar.
" Plug 'rickhowe/diffchar.vim', { 'on': 'TDChar' }
" Plug 'rickhowe/diffchar.vim'
"======================================================================================}}}

"vim-enhanced{{{==========================================================================
" :PatienceDiff - Use the Patience Diff algorithm for the next diff mode
" :EnhancedDiff <algorithm> - Use <algorithm> to generate the diff. Use any of
"     myers Default Diff algorithm used
"     default Alias for myers algorithm
"     histogram Fast version of patience algorithm
"     minimal Default diff algorithm, trying harder to minimize the diff
"     patience Patience diff algorithm.
" Note: Those 2 commands use the git command line tool internally
" to generate the diffs. Make sure you have at least git
" version 1.8.2 installed.
" :EnhancedDiffDisable - Disable plugin (and use default Vim diff capabilities).
" Plug 'chrisbra/vim-diff-enhanced', { 'on': 'EnhancedDiff' }
Plug 'chrisbra/vim-diff-enhanced'
"======================================================================================}}}

"linediff{{{==============================================================================
Plug 'AndrewRadev/linediff.vim', { 'on': 'Linediff' }
"======================================================================================}}}
