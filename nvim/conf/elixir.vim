"alchemist{{{=============================================================================
" This plugin uses alchemist-server to give inside
" information about your elixir project in vim.
Plug 'slashmili/alchemist.vim', { 'for': 'elixir' }
"======================================================================================}}}

"vim-elixir{{{============================================================================
Plug 'elixir-editors/vim-elixir', { 'for': 'elixir' }
"======================================================================================}}}

"vim-phoenix{{{===========================================================================
" Integrates with vim-projectionist for navigating project
" files according to Phoenix conventions.
" Comes with extra mappings for vim-surround.
" Comes with extra snippets for UltiSnips.
" Provides :Mix command that delegates to mix.
" Provides project-specific Vim navigation settings
" for path and suffixesadd.
Plug 'avdgaag/vim-phoenix', { 'for': 'elixir' }
"======================================================================================}}}

"vim-mix{{{===============================================================================
" Plugin for using Elixir's build tool, mix.
" :Mix [command] runs the default mix task (the "run" task, unless otherwise configured), or the given command.
" :Mclean cleans generated application files
" :Mcompile compiles sources files
" :Mdeps lists project dependencies and their status
" :Mdeps clean removes dependency files
" :Mdeps compile compiles dependencies
" :Mdeps get gets all out of date dependencies
" :Mdeps unlock unlocks all dependencies
" :Mdeps update updates project dependencies
" :Mtest runs the project's tests
" Planned commands
" :Mixfile opens the project Mixfile.
" :Mdeps unlock <deps> unlocks specific dependencies
" :Mdeps update <deps> updates specific dependencies
" :Mdo executes the commands separated by comma
" :Mescript generates an escript and replaces the current buffer
" :Mrun runs the given expression
Plug 'mattreduce/vim-mix', { 'for': 'elixir' }
"======================================================================================}}}
