"vim-xtract{{{============================================================================
" vim-xtract helps you split up large files
" into smaller files. Great for refactoring.
Plug 'rstacruz/vim-xtract', { 'on': 'Xtract'}
"======================================================================================}}}

"nrrwrgn{{{===============================================================================
" This plugin is inspired by the Narrowing feature
" of Emacs and means to focus on a selected region
" while making the rest inaccessible. You simply
"   select the region, call :NR and the selected
"   part will open in a new split window while the
"   rest of the buffer will be protected. Once you are
"   finished, simply write the narrowed window (:w)
"   and all the changes will be moved back to the original buffer.
" Ex commands:
" :NR      - Open the selected region in a new narrowed window
" :NW      - Open the current visual window in a new narrowed window
" :WidenRegion - (In the narrowed window) write the changes back to the original buffer.
" :NRV     - Open the narrowed window for the region that was last visually selected.
" :NUD     - (In a unified diff) open the selected diff in 2 Narrowed windows
" :NRP     - Mark a region for a Multi narrowed window
" :NRM     - Create a new Multi narrowed window (after :NRP)
" :NRS     - Enable Syncing the buffer content back (default on)
" :NRN     - Disable Syncing the buffer content back
" :NRL     - Reselect the last selected region and open it again in a narrowed window
Plug 'chrisbra/NrrwRgn', { 'on': 'NR' }
"======================================================================================}}}

"visual-split{{{==========================================================================
" 1.Visually select the lines you want to split out
" 2.Type :VSSplit, :VSSplitAbove or :VSSplitBelow to create the split
" 3.A new split will be created showing the selected lines
" <C-W>gr - resize split to visual selection (similar to :VSResize)
" <C-W>gss - split out visual selection (similar to :VSSplit)
" <C-W>gsa - split out visual selection above (similar to :VSSplitAbove)
" <C-W>gsb - split out visual selection below (similar to :VSSplitBelow)
Plug 'wellle/visual-split.vim', { 'on': 'VSSplit'}
"======================================================================================}}}
