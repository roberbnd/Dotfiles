"vim-devicons{{{==========================================================================
"cd ~/.fonts && curl -fLo DroidSansMonoForPowerlinePlusNerdFileTypes.otf
"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/patched-fonts
"/DroidSansMono/Droid%20Sans%20Mono%20for%20Powerline%20Plus%20Nerd%20File%20Types.otf
Plug 'ryanoasis/vim-devicons'
"======================================================================================}}}

"lightline{{{=============================================================================
" A light and configurable statusline/tabline plugin for Vim
Plug 'itchyny/lightline.vim'
Plug 'itchyny/vim-gitbranch'
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }
"======================================================================================}}}

"taboo{{{=================================================================================
" Rename tabs
Plug 'gcmt/taboo.vim'
"======================================================================================}}}

"rigel{{{=================================================================================
" Colorscheme
Plug 'rigellute/rigel'
"======================================================================================}}}

"seiya{{{=================================================================================
"transparent background color in (transparent) terminal.
Plug 'miyakogi/seiya.vim'

let g:seiya_auto_enable=1
let g:seiya_target_groups = has('nvim') ? ['guibg'] : ['ctermbg']
"======================================================================================}}}

"vim-brightest{{{=========================================================================
" bright the same words
Plug 'osyo-manga/vim-brightest'
let g:brightest#highlight = {
\   "group" : "ErrorMsg"
\}
"======================================================================================}}}

"vim-better-whitespace{{{===============================================================
" This plugin causes all trailing whitespace characters (see Supported Whitespace
" Characters below) to be highlighted.
Plug 'ntpeters/vim-better-whitespace'

let g:strip_only_modified_lines=1
"======================================================================================}}}

"vim-strip-trailing-whitespace{{{=========================================================
" Vim plugin that removes trailing whitespace
" from modified lines: Should not introduce extraneous changes into the diff, even when editing faulty files.
" For fixing up the whole file the command :StripTrailingWhitespace is provided.
" on save: Lines changing under you feet breaks any flow and is a compatibility hazard.
" Achieved by maintaining a set of all edited lines with trailing whitespace, backed by a Splay tree where children store line number offsets.
Plug 'axelf4/vim-strip-trailing-whitespace'
"======================================================================================}}}
