"vim-ruby-interpolation{{{================================================================
" Simple Vim plugin for Ruby which automatically
" adds {} when you type # inside string.
Plug 'p0deje/vim-ruby-interpolation', { 'for': 'ruby' }
"======================================================================================}}}

"vim-ruby{{{==============================================================================
" This project contains Vim configuration
" files for editing and compiling Ruby within Vim.
" Method [m ]m, Visual am,im,aM(Class)
Plug 'vim-ruby/vim-ruby', { 'for': 'ruby' }
"======================================================================================}}}

"vim-rails{{{===========================================================================
Plug 'tpope/vim-rails'
"======================================================================================}}}
