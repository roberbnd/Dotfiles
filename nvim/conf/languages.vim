"vim-sqlfmt{{{============================================================================
" vim plugin for SQL code format. similar to :GoFmt.
" :SQLFmt
Plug 'mattn/vim-sqlfmt', { 'on': 'SQLFmt' }
"======================================================================================}}}

"jenkinsfile-vim-syntax{{{================================================================
" Jenkinsfile DSL vim syntax
Plug 'martinda/Jenkinsfile-vim-syntax', { 'for': 'Jenkinsfile' }
"======================================================================================}}}

"dockerfile{{{============================================================================
" Vim syntax file for Docker's Dockerfile and
" snippets for snipMate.
Plug 'ekalinin/Dockerfile.vim', { 'for': 'Dockerfile' }
"======================================================================================}}}

"vim-crystal{{{===========================================================================
"This is filetype support for Crystal programming language.
"   crystal filetype detection
"   Syntax highlight
"   Indentation
"   vim-matchit support
"   crystal tool integration (implementations, context, formatter, and so on)
"   crystal spec integration
"   Syntax check (Using Syntastic)
"   Completion (currently for variable names)
Plug 'rhysd/vim-crystal', { 'for': 'crystal' }
"======================================================================================}}}

"mkdx{{{==================================================================================
" mkdx.vim is a markdown plugin that aims to reduce
" the time you spend formatting your markdown documents.
" It does this by adding some configurable mappings for
" files with a markdown filetype. Functions are included
" to handle lists, checkboxes (even lists of checkboxes!),
" fenced code blocks, shortcuts, headers and links.
" In addition to that, this plugin provides a mapping to
" convert a selection of CSV data to a markdown table.
" And there's a lot more :D Visit :h mkdx or
" :h mkdx-helptags for more information.
Plug 'SidOfc/mkdx', { 'for': 'markdown' }
"======================================================================================}}}
