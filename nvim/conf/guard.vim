" Notes
" delete all marks
" :delmarks A-Z0-9

set diffopt=filler,internal,algorithm:histogram,indent-heuristic,vertical
"vim-sandwich{{{==========================================================================
" The set of operator and textobject plugins
" to search/select/edit sandwiched textobjects.
"======================================================================================}}}
" Plug 'machakann/vim-sandwich'
"vim-jasmine{{{===========================================================================
"======================================================================================}}}

=========@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@===
"Description {===============================
" https://github.com/idanarye/vim-vebugger
" Run help vebugger-launching from Vim to learn
" how to launch the debugger.
" Run help vebugger-usage from Vim to learn
" how to operate the debugger.
"}===========================================
" Plug 'Shougo/vimproc.vim', {'do' : 'make'}
" Plug 'idanarye/vim-vebugger'

"Description {===============================
"}===========================================
Plug 'TaDaa/vimade'

"Description {===============================
"}===========================================
Plug 'Scuilion/markdown-drawer'

" for e in emoji#list()
"   call append(line('$'), printf('%s (%s)', emoji#for(e), e))
" endfor
"Description {===============================
" Argumentative aids with manipulating and moving
" between function arguments.
" Shifting arguments with <, and >,
" Moving between argument boundaries with [, and ],
" New text objects a, and i,
"============================================
Plug 'peterrincker/vim-argumentative'

"Description {===============================
"}===========================================
Plug 'rickhowe/spotdiff.vim'
"Description {===============================
"}===========================================
Plug 'junegunn/fzf', {'dir': '~/.fzf', 'do': './install --all'}
Plug 'fszymanski/fzf-gitignore', {'do': ':UpdateRemotePlugins'}
"Description {===============================
" The MatchTagAlways.vim (MTA) plug-in for
" the Vim text editor always highlights the
" XML/HTML tags that enclose your cursor location.
"}===========================================
" Plug 'valloric/MatchTagAlways', { 'for': 'html' }

"Description {===============================
" For example, below is the current content:
" <table|
" Now you press >, the content will be:
" <table>|</table>
" And now if you press > again, the content will be:
" <table>
"     |
" </table>
"}===========================================
Plug 'alvan/vim-closetag', { 'for': 'html' }
"Description {===============================
"}===========================================
" Plug 'pangloss/vim-javascript'

"Description {===============================
" angular-cli.vim provides a support for
" angular-cli, directly inside Vim.
"}===========================================
Plug 'bdauria/angular-cli.vim', { 'for': 'javascript' }

"Description {===============================
"}===========================================
Plug 'myhere/vim-nodejs-complete', { 'for': 'javascript' }

"Description {===============================
"}===========================================
Plug 'yardnsm/vim-import-cost', { 'do': 'npm install', 'for': 'javascript' }

"Description {===============================
" jsdoc.vim generates JSDoc block comments
" based on a function signature.
"}===========================================
Plug 'heavenshell/vim-jsdoc', { 'for': 'javascript' }
"Description {===============================
" This is a vim syntax plugin for Ansible 2.0, it supports YAML playbooks, Jinja2 templates, and Ansible's hosts files.
"  YAML playbooks are detected if:
"      they are in the group_vars or host_vars folder
"      they are in the tasks, roles, or handlers folder and have either a .yml or .yaml suffix
"      they are named playbook.y(a)ml, site.y(a)ml, or main.y(a)ml
"  Jinja2 templates are detected if they have a .j2 suffix
"  Files named hosts will be treated as Ansible hosts files
" You can also set the filetype to ansible, ansible_template, or ansible_hosts if auto-detection does not work (e.g. :set ft=ansible). Note: If you want to detect a custom pattern of your own, you can easily add this in your .vimrc using something like this:
" au BufRead,BufNewFile */playbooks/*.yml set filetype=ansible
"}===========================================
Plug 'pearofducks/ansible-vim'

"Description {===============================
" Things you can do with fzf and Vim.
"}===========================================
Plug 'junegunn/fzf.vim'

"Description {===============================
" Julia support for Vim.
""}===========================================
" Plug 'JuliaEditorSupport/julia-vim', { 'for': 'julia' }

"Description {===============================
" This vim bundle adds advanced syntax highlighting for GNU as (AT&T).
" This file defines a (almost) complete syntax for GNU as assembler.
" My motivation in writing this was the lack of a complete, working syntax for this common assembler.
" For now the basic as directives as well as the Intel / AMD, ARM (thumb) and AVR instruction sets are included.
" Opcodes supporting a size suffix are recognized plain as well as suffixed with b/w/l/q.
"}===========================================
Plug 'Shirk/vim-gas'

"Description {===============================
" Distinct highlighting of keywords vs values,
" JSON-specific (non-JS) warnings, quote concealing.
"}===========================================
" Plug 'elzr/vim-json', { 'for': 'json' }

"Description {===============================
" Trys to be smart by detecting a Repository and if
" that fails it checks the current directory
" nnoremap <silent> <leader>ff :DmenuFinderFindFile<cr>
"
" " Find file in current directory
" nnoremap <silent> <leader>fd :DmenuFinderCurDir<cr>
"
" " find file in repository
" nnoremap <silent> <leader>fr :DmenuFinderRepo<cr>
"
" " The plugin caches the files in the repository and
" " subsequently searches those. You can clear that cache
" " if new files are not showing up.
" nnoremap <silent> <leader>fc :DmenuFinderClearCache<cr>
"
" " Find file in open buffers
" nnoremap <silent> <leader>fb :DmenuFinderFindBuffer<cr>
"
" " Location of dmenu command
" let g:dmenu_finder_dmenu_command = "/usr/bin/dmenu"
"}===========================================
Plug 'has2k1/vim-dmenu-finder'

"Description {===============================
"}===========================================
Plug 'kronos-io/kronos.vim', { 'on': 'Kronos'}
" Ultisnips
"==========================================
let g:UltiSnipsSnippetsDir='~/.config/nvim/UltiSnips'
let g:ycm_min_num_of_chars_for_completion = 1
let g:UltiSnipsExpandTrigger="<c-c>"
let g:UltiSnipsJumpForwardTrigger="<F9>"
let g:UltiSnipsJumpBackwardTrigger="<F8>"
let g:python_host_prog = '/usr/bin/python2.7'
let g:python3_host_prog = '/usr/bin/python'

"Description {===============================
" UltiSnips - The ultimate snippet solution for Vim.
"}===========================================
Plug 'SirVer/ultisnips'

"Description {===============================
" Make the yanked region apparent!
"}===========================================
" Plug 'machakann/vim-highlightedyank'

"Description {===============================
" Search Ag [options] {pattern} [{directory}]
"}===========================================
Plug 'rking/ag.vim'

"Description {===============================
" Fuzzy file finding for neovim, via fzy[1].
" :FuzzyOpen
"}===========================================
" Plug 'cloudhead/neovim-fuzzy'

"Description {===============================
" You can do SSH to a server via ctrlp.vim interface
" using this plugin. Note that this plugin is a ctrlp.vim
" extension and it requires a runner (tmux is currently only supported).
" :CtrlPSSH
"}===========================================
" Plug 'tacahiroy/ctrlp-ssh'

"Description {===============================
" This extension adds a new CtrlP command,
" the :CtrlPCmdPalette, which allows you
" to find and run vim commands (internal or custom).
"  the :CtrlPCmdPalette, w
"}===========================================
" Plug 'fisadev/vim-ctrlp-cmdpalette'

"Description {===============================
" Easily open locally modified files in your
" git-versioned projects.
" The idea is that you're likely to be editing
" the same files again, or maybe you just want to
" catch up on the progress you've made after
" coming back from a break.
" :CtrlPModified shows all files which have been
" modified since your last commit.
" :CtrlPBranch shows all files modified on your
" current branch.
" Then set yourself up some mappings:
" map <Leader>m :CtrlPModified<CR>
" map <Leader>M :CtrlPBranch<CR>
"}===========================================
" Plug 'jasoncodes/ctrlp-modified.vim'

"Description {===============================
" With the vim plugin CtrlP you can easily open a
" file or a buffer. This is a ctrlp.vim extension
" that allow you to switch between different opened tabs.
" Put this into your vimrc so the SmartTabs
" search will show up when you open CtrlP:
" let g:ctrlp_extensions = ['smarttabs']
" Other options:
" let g:ctrlp_smarttabs_modify_tabline = 0
" " If 1 will highlight the selected file in the tabline.
" " (Default: 1)
" let g:ctrlp_smarttabs_reverse = 0
" Reverse the order in which files are displayed.
" (Default: 1)
"}===========================================
" Plug 'DavidEGx/ctrlp-smarttabs'

" Auto-pairs
"============================================
let g:AutoPairsMapSpace = 0

"Description {===============================
" A super simple function navigator for ctrlp.vim.
" For lazy people who cannot wait until ctags finishes.
"}===========================================
" Plug 'tacahiroy/ctrlp-funky'

"Description {===============================
" Basic Usage
" Move cursor to the Class/Method usage in your code
" Press c-] (if you have created mapping) or just
" execute :CtrlPtjump (or :CtrlPtjumpVisual in
" visual mode) in the command line.
" Or provide the symbol as an argument:
" :CtrlPtjump MyFavoriteClass
"}===========================================
" Plug 'ivalkeen/vim-ctrlp-tjump'



"Description {===============================
" Easily interact with tmux from vim.
"}===========================================
Plug 'renmills/vimux'

"Description {===============================
" vimux with Elixir mix integration
"}===========================================
Plug 'spiegela/vimix'

"Description {===============================
" This a vim plugin that enables MATLAB-style
" cell mode execution for python scripts in vim,
" assuming an ipython interpreter running
" in screen (or tmux).
"}===========================================
Plug 'julienr/vim-cellmode'
"Description {===============================
" Easily interact with tmux from vim.
"}===========================================
Plug 'renmills/vimux'

"Description {===============================
" vimux with Elixir mix integration
"}===========================================
Plug 'spiegela/vimix'

"Description {===============================
" This a vim plugin that enables MATLAB-style
" cell mode execution for python scripts in vim,
" assuming an ipython interpreter running
" in screen (or tmux).
"}===========================================
Plug 'julienr/vim-cellmode'
"Description {===============================
" To show Github issues for the current repository:
"}===========================================
Plug 'jaxbot/github-issues.vim', { 'on': 'Gissues' }

"Description {===============================
" To simply test vimagit, modify/add/delete/rename
" some files in a git repository and open vim.
":Magit
"Open magit buffer with :Magit command.
"<C-n>
"Jump to next hunk with <C-n>, or move the cursor
"as you like. The cursor is on a hunk.
"S
"While the cursor is on an unstaged hunk,
"press S in Normal mode: the hunk is now staged,
"and appears in "Staged changes" section (you can
"also unstage a hunk from "Staged section" with S).
"CC
"Once you have stage all the required changes, press CC.
"    Section "Commit message" is shown.
"    Type your commit message in this section.
"    To commit, go back in Normal mode, and press CC (or :w if you prefer).
"}===========================================
Plug 'jreybert/vimagit'



"Description {===============================
" Automatically save and load vim session based on switching of git branch
" Commands
" - Gsw[!] <branch name>
" Switch branches. When switching branches, If the
" session that the same name as the branch has been
" saved, the session is loaded. With a '!' bang, it
" only switch branches. If there is no switching
" destination branch, create new branch and switch to it.
" This command is equipped with branch names completion.
" - GswRemote[!] <branch name>
" Checkout a remote branch. At that time, if the
" session that the same name as the remote branch has been saved,
" the session is loaded. With a '!' bang, it only checkout a remote
" branch. This command is equipped with remote branch names completion.
" - GswPrev[!]
" Switch to the previous branch that switched by git-switcher.
" The operation except switching to the previous branch is the same as the Gsw command.
" - GswSave [session-name]
" If this command is run with no arguments, then save the
" session in the current working branch name. With an argument,
" then save the given string as the session name. This command is
" equipped with session names completion that has already been saved.
" In addition, this command works even if it is not in the git
" managed directory, that case is to save the session using
" g:gsw_non_project_sessions_dir and g:gsw_non_project_default_session_name option.
" - GswLoad [session-name]
" If this command is run with no arguments, then load the
" session in the current working branch name. With an argument,
" then load the given string as the session name. This command is
" equipped with saved session names completion. In addition, this
" command works even if it is not in the git managed directory,
" that case is to load the session using g:gsw_non_project_sessions_dir
" and g:gsw_non_project_default_session_name option.
" - GswMove[!] <branch name>
" This command move(rename) the current working branch.
" With a '!' bang, to move without confirmation.
" - GswRemove[!] <branch-name>
" This command remove specified branch. With a '!' bang,
" to remove without confirmation.
" - GswSessionList
" This command display a list of saved session names.
" - GswPrevBranchName
" This command display the name of the previous
" branch that switched by git-switcher.
" - GswClearState
" This command initialize Vim's window, tab, buffer.
" - GswDeleteSession[!] <session-name>
" This command remove the specified session after confirming.
" With a '!' bang, to remove without confirmation.
" This command is equipped with saved session names completion.
" - GswDeleteSessionsIfBranchNotExist[!]
" This command remove saved sessions there is no branch of
" the same name after confirming in the local repositry.
" With a '!' bang, to remove without confirmation.
" - GswBranch
" This command display a list of branch in the local repositry.
" - GswBranchRemote
" This command display a list of branch in the remote repositry.
" - GswFetch
" Execute git fetch.
" - GswPull
" Execute git pull.
"}===========================================
Plug 'ToruIwashita/git-switcher.vim'

"Description {===============================
" vim-git-log is a Vim plugin that helps
" browse your git log. This plugin requires
" Fugitive.
" :GitLog
"}===========================================
Plug 'kablamo/vim-git-log'

"Description {===============================
" it is essentially a wrapper around git log --graph,
" allowing you to see your branching history.
"}===========================================
Plug 'gregsexton/gitv'


\
"Description {===============================
" Open the commit graph with :Flog or :Flogsplit.
" Many options can be passed in, complete with <Tab> completion.
"
" Preview commits once you've opened Flog using <CR>.
" Jump between commits with <C-N> and <C-P>.
"
" Refresh the graph with u. Toggle viewing all
" branches with a. Toggle bisect mode with gb.
" Toggle displaying no merges with gm. Quit with ZZ.
"
" If you forget these bindings, press g? at any
" time to get help.

"Description {===============================
" Browse GitHub events
" (user dashboard, user/repo activity) in Vim.
" :GHD! matz
" With authentication
"     :GHDashboard
"     :GHDashboard USER
"     :GHActivity
"     :GHActivity USER
"     :GHActivity USER/REPO
" Without authentication (60 calls/hour limit,
" only public activities)
"     :GHDashboard! USER
"     :GHActivity! USER
"     :GHActivity! USER/REPO
"}===========================================
Plug 'junegunn/vim-github-dashboard', { 'on': [
      \'GHDashboard', 'GHActivity' ]}
"
"Vimux {{{====================================
" nnoremap <leader>rr :call VimuxPromptCommand()<cr>
" nnoremap <leader>rl :call VimuxRunLastCommand()<cr>
" nnoremap <leader>rc :call VimuxCloseRunner()<cr>
"}}}==========================================

"Fuzzy {{{====================================
" nnoremap <leader>fa :FuzzyOpen<cr>
" nnoremap <leader>fo :FuzzyGrep<cr>
"}}}==========================================


"Description {===============================
" The NERDTree is a file system explorer for the
" Vim editor. Using this plugin, users can visually
" browse complex directory hierarchies, quickly
" open files for reading or editing, and perform
" basic file system operations.
"}===========================================
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tiagofumo/vim-nerdtree-syntax-highlight', { 'on': 'NERDTreeToggle'}
"Description {===============================
" ALE (Asynchronous Lint Engine) is a plugin for
" providing linting (checking syntax and semantics)
" in NeoVim 0.2.0+ and Vim 8 while you edit your text
" files, and acts as a Vim Language Server Protocol client.
"}===========================================
" Plug 'dense-analysis/ale'

" Description {==============================
" EditorConfig helps maintain consistent
" coding styles for multiple developers
" working on the same project across
" various editors and IDEs.
"}===========================================
" Plug 'editorconfig/editorconfig-vim'

Plug 'DavidEGx/ctrlp-smarttabs'
"CtrlP {{{====================================
nnoremap ra :CtrlP<cr>
nnoremap ro :CocList lines<cr>
nnoremap rr :CocList outline<cr>
nnoremap ru :CtrlPBuffer<cr>
nnoremap rt :CtrlPSmartTabs<cr>
"}}}==========================================
"Description {===============================
"}===========================================
" Plug 'janko/vim-test'



" Vimux
"============================================
let g:VimuxHeight = "30"
let g:VimuxOrientation = "h"
let g:VimuxRunnerType = "pane"
let g:VimuxUseNearest = 0

" Neomake
"==========================================
" https://www.gregjs.com/vim/2015/linting-code-with-neomake-and-neovim/
let g:neomake_javascript_jscs_maker = {
    \ 'exe': 'standard',
    \ 'errorformat': '%f: line %l\, col %c\, %m',
    \ }
let g:neomake_javascript_enabled_makers = ['standard']

let g:neomake_python_flake8_maker = {
    \ 'args': ['--ignore=E221,E241,E272,E251,W702,E203,E201,E202',  '--format=default'],
    \ 'errorformat':
        \ '%E%f:%l: could not compile,%-Z%p^,' .
        \ '%A%f:%l:%c: %t%n %m,' .
        \ '%A%f:%l: %t%n %m,' .
        \ '%-G%.%#',
    \ }
let g:neomake_python_enabled_makers = ['flake8']

let g:neomake_warning_sign = {
  \ 'text': '⚠️ ',
  \ 'texthl': 'WarningMsg',
  \ }
let g:neomake_error_sign = {
  \ 'text': '❌',
  \ 'texthl': 'ErrorMsg',
  \ }

"ALE
"==========================================
let g:ale_enabled = 1
let g:ale_sign_column_always = 1
let g:ale_completion_enabled = 1
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = 0
let g:ale_lint_on_save = 1
let g:ale_html_tidy_options = '--custom-tags blocklevel'
let g:ale_fixers = {
      \ 'javascript': ['prettier', 'standard'],
      \ 'python': 'yapf',
      \}

"Description {===============================
" map <silent> <Leader>he :call HtmlEscape()<CR>
" map <silent> <Leader>hu :call HtmlUnEscape()<CR>
"}===========================================
" Plug 'skwp/vim-html-escape', { 'for': 'html' }

"Description {===============================
" HTML5 + inline SVG omnicomplete function, indent
" and syntax for Vim. Based on the default htmlcomplete.vim.
"}===========================================
" Plug 'othree/html5.vim', { 'for': 'html' }

"Description {===============================
" Add CSS3 syntax support to Vim's built-in syntax/css.vim.
"}===========================================
" Plug 'hail2u/vim-css3-syntax', { 'for': 'html' }
"Description {===============================
" The main entry point of the plugin is a single command,
" :Switch. When the command is executed, the plugin looks
" for one of a few specific patterns under the cursor and
" performs a substitution depending on the pattern.
" For example, if the cursor is on the "true" in
" the following code:
" flag = true
" Then, upon executing :Switch, the "true"
" will turn into "false".
"}===========================================
Plug 'AndrewRadev/switch.vim'
" au BufEnter *.sh imap jxt <space>?=<space>
" au BufEnter *.sh imap jxn <space>:=<space>
" au BufEnter *.hs imap jxt <space>::<space>
" au BufEnter *.hs imap jxn <space>$<space>
" au BufEnter *.hs imap jxs <space>!!<space>
" au BufEnter *.hs imap jxg <space><bar><space>
" au BufEnter *.hs imap jxc <space>++<space>
" au BufEnter *.scala imap jxt <space><-<space>

"Tags {{{=====================================
" nnoremap cu <c-]>
" nnoremap ce <c-w><c-]>
" nmap co :vsp <CR>:exec("tag ".expand("<cword>"))<CR>
" nmap cd :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
" nmap cq <c-T>
"}}}==========================================

"Description {===============================
"DELETED BECAUSE IS SLOW TO OPEN FILES
" help you read complex code by showing diff
" level of parentheses in diff color !!
"}===========================================
" Plug 'luochen1990/rainbow'
" Description {==============================
" This plugin causes all trailing whitespace characters
" let g:better_whitespace_ctermcolor='<desired_color>'
" To enable highlighting and stripping whitespace on save by default
" let g:better_whitespace_enabled=1
" let g:strip_whitespace_on_save=1
" :EnableWhitespace
" :DisableWhitespace
" :ToggleWhitespace
" To clean extra whitespace, call:
" :StripWhitespace
"}===========================================
" Plug 'ntpeters/vim-better-whitespace'

" Description {==============================
"}===========================================
Plug 'axelf4/vim-strip-trailing-whitespace'
" Plug 'vim-airline/vim-airline'
" Airline
"==========================================
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#fnamemod = ':t:r'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#ale#enabled = 1
let g:airline_section_error = '%{airline#util#wrap(airline#extensions#coc#get_error(),0)}'
let g:airline_section_warning = '%{airline#util#wrap(airline#extensions#coc#get_warning(),0)}'
"Description {===============================
"}===========================================
" Plug 'farmergreg/vim-lastplace'


" Description {==============================
"}===========================================
Plug 'jlanzarotta/bufexplorer'
"BufExplorer
"==========================================
let g:bufExplorerDisableDefaultKeyMapping=1

"I prefer CLAP BUFFERS
" "Description {===============================
" "}===========================================
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'bogado/file-line'

" Switch
"============================================
let g:switch_mapping = "za"

" Switch
"==========================================
let g:switch_mapping = ""

"Rainbow
"==========================================
let g:rainbow_active = 1
" JSdoc
"============================================
let g:jsdoc_return = 0
let g:jsdoc_input_description = 1
let g:jsdoc_allow_input_prompt = 1
" GitSession
"============================================
let g:gitsessions_dir = '~/.config/nvim/gitsessions/'
" Closetag
"==========================================
let g:closetag_filenames = "*.html,*.xhtml,*.phtml"
" Color indicator of unsaved buffer
function! AirlineInit()
  " first define a new part for modified
  call airline#parts#define('modified', {
    \ 'raw': '%m',
    \ 'accent': 'red',
    \ })

  " then override the default layout for section c with your new part
  let g:airline_section_c = airline#section#create(['%<', '%f', 'modified', ' ', 'readonly'])
endfunction
"tmuxline{{{==============================================================================
" Simple tmux statusline generator with support
" for powerline symbols and vim/airline/lightline
" statusline integration
Plug 'edkolev/tmuxline.vim'
"======================================================================================}}}
"lightline-bufferline{{{==================================================================
" This plugin provides bufferline functionality for the lightline vim plugin.
Plug 'mengelbrecht/lightline-bufferline'
let g:lightline#bufferline#show_number  = 1
let g:lightline#bufferline#shorten_path = 0
let g:lightline#bufferline#unnamed      = '[No Name]'

let g:lightline                  = {}
let g:lightline.tabline          = {'left': [['buffers']], 'right': [['close']]}
let g:lightline.component_expand = {'buffers': 'lightline#bufferline#buffers'}
let g:lightline.component_type   = {'buffers': 'tabsel'}
let g:lightline#bufferline#enable_devicons = 1
"======================================================================================}}}
"lightline-bufferline{{{==================================================================
autocmd BufWritePost,TextChanged,TextChangedI * call lightline#update()
"======================================================================================}}}
"vim-fugitive{{{==========================================================================
" If some of your projects have multiple environments
" (e.g. development, staging, production - with nearly
" the same directory and files structure), then there
" is a situations when you need to connect to one of
" this environments via ssh and remotely edit project-related files.
" :Gmove, Gremove, Gread, Gwrite, Gbrowse so on.
Plug 'tpope/vim-fugitive'
"======================================================================================}}}

"vim-merginal{{{==========================================================================
" Requires Fugitive :Merginal command
" Viewing the list of branches, Checking out branches from that list
" Creating new branches, Deleting branches Merging branches
" Rebasing branches, Solving merge conflicts
" Interacting with remotes(pulling, pushing, fetching, tracking)
" Diffing against other branches, Renaming branches
" Viewing git history for branches
" Plug 'idanarye/vim-merginal', { 'on': 'Merginal' }
"======================================================================================}}}

"gina{{{==================================================================================
" gina.vim is a git manipulation plugin which use
" a job features of Vim 8 or Neovim. It execute
" most of command in asynchronously so it won't
" lock your cursor. It is an alternative plugin
" of lambdalisue/vim-gita.
Plug 'lambdalisue/gina.vim', { 'on': 'Gina' }
"======================================================================================}}}

"agit{{{==================================================================================
" git log :Agit
Plug 'cohama/agit.vim', { 'on': 'Agit' }
"======================================================================================}}}

"vim-flog{{{==============================================================================
" Run :Git commands in a split next to the graph
" using :Floggit!. Command line completion is
" provided to do any git command with the
" commits and refs under the cursor.
Plug 'rbong/vim-flog', { 'on': 'Flog' }
"======================================================================================}}}
"Python fold
"============================================
" autocmd FileType python setlocal foldexpr=SimpylFold(v:lnum) foldmethod=expr
" autocmd FileType python setlocal foldexpr< foldmethod<
nnoremap S :bn<cr>
nnoremap T :bp<cr>
"Numbers {{{==================================
" inoremap 7 2
" inoremap 5 3
" inoremap 3 4
" inoremap 1 5
" inoremap 9 6
" inoremap 0 7
" inoremap 2 8
" inoremap 4 9
" inoremap 6 0
" inoremap 8 1
"
" tnoremap 7 2
" tnoremap 5 3
" tnoremap 3 4
" tnoremap 1 5
" tnoremap 9 6
" tnoremap 0 7
" tnoremap 2 8
" tnoremap 4 9
" tnoremap 6 0
" tnoremap 8 1
"
" cnoremap 7 2
" cnoremap 5 3
" cnoremap 3 4
" cnoremap 1 5
" cnoremap 9 6
" cnoremap 0 7
" cnoremap 2 8
" cnoremap 4 9
" cnoremap 6 0
" cnoremap 8 1
"}}}==========================================

nnoremap <F4> :StripWhitespace<cr>
" Output the current syntax group
nnoremap <F7> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<cr>
nnoremap <F8> :Limelight!!<CR>
nnoremap <F9> :TabooRename<space>
" nnoremap <F10> :!streamlink '<c-R>+' best &<cr><cr>
" nnoremap d{ f{%x<c-o>x
" nnoremap d( f(%x<c-o>x
nnoremap <leader>g, :diffget //2<cr>
nnoremap <leader>g. :diffget //3<cr>
nmap <leader>ga szga
nmap <leader>gr W<bs><cr>
"change ( to {"
" nnoremap c( f(%r}<c-o>r{
"change { to ("
" nnoremap c{ f{%r)<c-o>r(
"BufExplorer {{{==============================
nmap <leader>. :ToggleBufExplorer<cr><Plug>(easymotion-s2)
"}}}==========================================

"Gina {{{=====================================
nnoremap <leader>gg :Gina status<cr>
nnoremap <leader>gc :Gina chaperon<cr>
"}}}==========================================
imap q. <bs><bs><bs>
"conflict-marker{{{=======================================================================
" [x and ]x mappings are defined as default.
" Jump within a Conflict Marker This feature uses matchit.vim, which is bundled
" in Vim (macros/matchit.vim). % mapping is extened by matchit.vim.
" This plugin defines mappings as default,
" ct for themselves,
" co for ourselves,
" cn for none and
" cb for both.
" <<<<<<< HEAD
" ourselves
" =======
" themselves
" >>>>>>> deadbeef0123
Plug 'rhysd/conflict-marker.vim'
"======================================================================================}}}
"blamer{{{================================================================================
":BlamerToggle
" https://www.reddit.com/r/neovim/comments/ep6hmt/
" blamernvim_a_git_blame_plugin_for_neovim_inspired/
Plug 'APZelos/blamer.nvim'
"======================================================================================}}}
"vim-jump{{{==============================================================================
" https://www.youtube.com/watch?v=zKwsWIMfs24
Plug 'wincent/vcs-jump'
"======================================================================================}}}
"vim-dirdiff{{{===========================================================================
" If you like Vim's diff mode, you would love to do this recursively on two directories!
Plug 'will133/vim-dirdiff'
"======================================================================================}}}
"gitignore{{{=============================================================================
" This plugin provides syntax highlighting, code snippets (using snipMate) based
" on https://github.com/github/gitignore snippets and supporing of tComment
" for .gitignore files in Vim.
Plug 'rdolgushin/gitignore.vim'
"======================================================================================}}}
" "Asterisk {{{=================================
" map *  <Plug>(asterisk-z*)
" map #  <Plug>(asterisk-z#)
" map g*  <Plug>(asterisk-g*)
" map g#  <Plug>(asterisk-g#)
" map z*  <Plug>(asterisk-z*)
" map gz* <Plug>(asterisk-gz*)
" map z#  <Plug>(asterisk-z#)
" map gz# <Plug>(asterisk-gz#)
" "}}}==========================================
" "asterisk{{{==============================================================================
" " asterisk.vim provides improved * motions.
" Plug 'haya14busa/vim-asterisk'
"
" let g:asterisk#keeppos = 1
" "======================================================================================}}}
"ctrlp{{{=================================================================================
" Run :CtrlP or :CtrlP [starting-directory] to invoke CtrlP in find file mode.
" Run :CtrlPBuffer or :CtrlPMRU to invoke CtrlP in find buffer or find MRU file mode.
" Run :CtrlPMixed to search in Files, Buffers and MRU files at the same time.
Plug 'ctrlpvim/ctrlp.vim'

let g:ctrlp_max_files=0
let g:ctrlp_max_depth = 40
let g:ctrlp_extensions = ['buffertag', 'tag', 'line', 'dir', 'tmux', 'mpc', 'smarttabs']
let g:ctrlp_working_path_mode = 'a'
let g:ctrlp_show_hidden = 1
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
"======================================================================================}}}
"vim-rails{{{=============================================================================
" Remember when everybody and their mother was
" using TextMate for Ruby on Rails development?
" Well if it wasn't for rails.vim, we'd still
" be in that era. So shut up and pay some respect.
" And check out these features:
Plug 'tpope/vim-rails', { 'for': 'ruby' }
"======================================================================================}}}

"vim-rbenv{{{=============================================================================
" This simple plugin provides a :Rbenv command
" that wraps !rbenv with tab complete. It also
" tells recent versions of vim-ruby where your
" Ruby installs are located, so that it can set
" 'path' and 'tags' in your Ruby buffers to
" reflect the nearest .ruby-version file.
Plug 'tpope/vim-rbenv', { 'for': 'ruby' }
"======================================================================================}}}

"vim-burdler{{{===========================================================================
" This is a lightweight bag of Vim goodies for Bundler,
" best accompanied by rake.vim and/or rails.vim. Features:
" :Bundle, which wraps bundle.
" An internalized version of bundle open: :Bopen (and :Bsplit, :Btabedit, etc.).
" 'path' and 'tags' are automatically altered to
" include all gems from your bundle. (Generate those tags with gem-ctags!)
" Highlight Bundler keywords in Gemfile.
" Support for gf in Gemfile.lock, plus syntax highlighting
" that distinguishes between installed and missing gems.
" Support for projectionist.vim, including
" projections based on which gems are bundled.
Plug 'tpope/vim-bundler', { 'for': 'ruby' }
"======================================================================================}}}
"vim-rake{{{==============================================================================
" Rake.vim is a plugin leveraging projectionist.vim
" to enable you to use all those parts of rails.vim
" that you wish you could use on your other Ruby
" projects, including :A, :Elib and friends, and
" of course :Rake. It's great when paired with gem
" open and bundle open and complemented nicely by bundler.vim.
Plug 'tpope/vim-rake', { 'for': 'ruby' }
"======================================================================================}}}

"vim-ruby-test{{{=========================================================================
" RunRubyFocusedTest - run focused test/spec
" RunRailsFocusedTest - run focused test (no spec support) in a Rails app
" RunRubyFocusedContext - run current context (rspec, shoulda)
" RunAllRubyTests - run all the tests/specs in the current file
Plug 'pgr0ss/vimux-ruby-test', { 'for': 'ruby' }
"======================================================================================}}}
"lessspace{{{=============================================================================
" This simple plugin will strip the trailing whitespace from the file you are editing.
" However, it does this only on the lines you edit or visit in Insert mode; it leaves all
" the others untouched.
" I wrote this plugin because I work with people whose editors don't clean up whitespace.
" My editor (Vim) did strip all whitespace, and saving my edits caused a lot of extraneous
" whitespace changes that showed up in version control logs.  This plugin gives the best
" of both worlds: it cleans up whitespace on edited lines only, keeping version control
" diffs clean.
" Plug 'thirtythreeforty/lessspace.vim'
"======================================================================================}}}
nnoremap <leader>cj :Dirvish %<cr>
nnoremap ck :RangerCurrentDirectory<cr>
nnoremap <leader>ck :Dirvish<cr>
nmap <leader>srj sm:Ranger<cr>
nmap <leader>srk sm:RangerWorkingDirectory<cr>
"vim-dirvish{{{=========================================================================
Plug 'justinmk/vim-dirvish'
"======================================================================================}}}

"ranger{{{================================================================================
" Ranger integration in vim and neovim
" install w3m
Plug 'rbgrouleff/bclose.vim'
Plug 'francoiscabrol/ranger.vim'

let g:ranger_map_keys = 0
"======================================================================================}}}

Plug 'kevinhwang91/rnvimr'
" let g:rnvimr_layout = { 'relative': 'editor',
"             \ 'width': &columns,
"             \ 'height': &lines,
"             \ 'col': &columns,
"             \ 'row': &lines,
"             \ 'style': 'minimal' }

let g:rnvimr_ex_enable = 1
nnoremap cj :Ranger<cr>
nnoremap ck :RangerCurrentDirectory<cr>
nnoremap <leader>cj :Dirvish %<cr>
nnoremap <leader>ck :Dirvish<cr>
nmap <leader>srj sm:Ranger<cr>
nmap <leader>srk sm:RangerWorkingDirectory<cr>

nnoremap rk :Bclose<cr><c-w>c
nnoremap rj :Bclose<cr>
Plug 'bfredl/nvim-miniyank'
"flyGrep{{{===============================================================================
" <Esc> 	close FlyGrep buffer
" <C-c> 	close FlyGrep buffer
" <Enter> 	open file at the cursor line
" <Tab> 	move cursor line down
" <C-j> 	move cursor line down
" <S-Tab> 	move cursor line up
" <C-k> 	move cursor line up
" <Bs> 	remove last character
" <C-w> 	remove the Word before the cursor
" <C-u> 	remove the Line before the cursor
" <C-k> 	remove the Line after the cursor
" <C-a>/<Home> 	Go to the beginning of the line
" <C-e>/<End> 	Go to the end of the line
Plug 'wsdjeg/FlyGrep.vim'
"======================================================================================}}}
"FlyGrep{{{====================================
"Search in project
":Far foo bar **/*.py
nnoremap <leader>sl :FlyGrep<cr>
"}}}==========================================


"ctrlsf{{{====================================
"Search in project
nnoremap <leader>sa :CtrlSF<space>
nnoremap <leader>so yiw:CtrlSF<space><c-r>"
nnoremap <leader>se :CtrlSFToggle<cr>
"}}}==========================================

Plug 'machakann/vim-sandwich'
command! -bang -nargs=? -complete=dir Files
    \ call fzf#vim#files(<q-args>, {'options': ['--layout=reverse', '--info=inline', '--preview', '~/.config/nvim/plugged/fzf.vim/bin/preview.sh {}']}, <bang>0)
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

let g:fzf_layout = { 'window': 'call CreateCenteredFloatingWindow()' }
"vim-win{{{===============================================================================
Plug 'wesQ3/vim-windowswap'
"======================================================================================}}}
"vim-alias{{{=============================================================================
" This plugin lets you define command-line abbreviations by
" :Alias which only expand at the beginning of the command prompt.
" :Alias [-range] [-buffer] <lhs> <rhs>
" :UnAlias <lhs> ...
" :Aliases [<lhs> ...]
Plug 'konfekt/vim-alias'
"======================================================================================}}}
autocmd VimEnter * source ~/.config/nvim/conf/alias.vim

"vim-esearch{{{===========================================================================
" NeoVim/Vim plugin performing project-wide
" async search and replace, similar to
" SublimeText, Atom et al.
Plug 'eugen0329/vim-esearch'
"======================================================================================}}}



"ctrlsf{{{================================================================================
" In CtrlSF window:
"     Enter, o, double-click - Open corresponding file
"     of current line in the window which CtrlSF is launched from.
"     <C-O> - Like Enter but open file in a horizontal split window.
"     t - Like Enter but open file in a new tab.
"     p - Like Enter but open file in a preview window.
"     P - Like Enter but open file in a preview window and switch focus to it.
"     O - Like Enter but always leave CtrlSF window opening.
"     T - Like t but focus CtrlSF window instead of new opened tab.
"     M - Switch result window between normal view and compact view.
"     q - Quit CtrlSF window.
"     <C-J> - Move cursor to next match.
"     <C-K> - Move cursor to previous match.
"     <C-C> - Stop a background searching process.
"
" In preview window:
"
"     q - Close preview window.
Plug 'dyng/ctrlsf.vim'
"======================================================================================}}}

"vim-simple-todo{{{=======================================================================
" It adds some useful mappings for manage
" simple todo lists
Plug 'vitalk/vim-simple-todo'

let g:simple_todo_map_keys = 0
"======================================================================================}}}

"todo-vim{{{==============================================================================
" todo-vim is plugin for manage your todo notes.
" He will help you to navigate and manage
" your todo labels in code
" nmap <F5> :TODOToggle<CR>
Plug 'Dimercel/todo-vim', { 'on': 'TODOToggle'}
"======================================================================================}}}
"Simple ToDo {{{==============================
map <leader>t; <Plug>(simple-todo-new-start-of-line)
map <leader>t, <Plug>(simple-todo-new)
map <leader>t. <Plug>(simple-todo-mark-switch)
map <leader>tp :Note Do<cr>
"}}}==========================================
"Testing {{{==================================
nmap <leader>nt :TestFile<cr>
nmap <leader>na :TestNearest<cr>
nmap <leader>no :TestSuite<cr>
nmap <leader>ne :TestLast<cr>
nmap <leader>nu :TestVisit<cr>
"}}}==========================================

" ie = inner entire buffer
onoremap ie :exec "normal! ggVG"<cr>
" iv = current viewable text in the buffer
onoremap iv :exec "normal! HVL"<cr>
"vim-swap{{{==============================================================================
" reorder arguments of a function
" three key mappings in default, g<, g>, gs mode.
Plug 'machakann/vim-swap'
"======================================================================================}}}
"vim-tmux-navigator{{{====================================================================
" This plugin provides the following mappings which allow
" you to move between Vim panes and tmux splits seamlessly.
"     <ctrl-h> => Left
"     <ctrl-j> => Down
"     <ctrl-k> => Up
"     <ctrl-l> => Right
"     <ctrl-\> => Previous split
Plug 'christoomey/vim-tmux-navigator'

if &term =~ '^screen'
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif
let g:tmux_navigator_no_mappings = 1
"======================================================================================}}}

"tmux-complete{{{=========================================================================
" <C-X><C-U> to the trigger user defined insert mode completion.
Plug 'junegunn/tmux-complete.vim'
"======================================================================================}}}


"vim-ragtag{{{============================================================================
" A set of mappings for HTML, XML, PHP, ASP,
" eRuby, JSP, and more (formerly allml)
Plug 'tpope/vim-ragtag'
"======================================================================================}}}
"nvim-gdb{{{==============================================================================
"DEBUG
Plug 'sakhnik/nvim-gdb'
Plug 'puremourning/vimspector'
"======================================================================================}}}
"limelight{{{=============================================================================
" Hyperfocus-writing in Vim.
Plug 'junegunn/limelight.vim', { 'on': 'Limelight'}
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

"Color name (:help gui-colors) or RGB color
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#777777'
"======================================================================================}}}

"tcd{{{===================================================================================
" Tcd lets you set a working directory for a specific tab. Great if you've
" got a session open and are editing some file in another location on the
" filesystem, and decide you need to open more files in that directory.
" This plugin works particularly well in combination with NERD_Tree.vim
Plug 'vim-scripts/tcd.vim', { 'on': 'Tcd'}
"======================================================================================}}}
"vim-IndentConsistencyCop{{{==============================================================
" In order to achieve consistent indentation, you need to agree on the indentation width
" (e.g. 2, 4 or 8 spaces), and the indentation method
Plug 'inkarkat/vim-IndentConsistencyCop', { 'on': 'IndentConsistencyCop' }
"======================================================================================}}}
"vCoolor{{{===============================================================================
" In GNU/Linux it uses a simple GTK+ dialog
" via Zenity or Yad.
" Select color
Plug 'KabbAmine/vCoolor.vim'
let g:vcoolor_disable_mappings = 1
ranger
"======================================================================================}}}
Plug 'kevinhwang91/rnvimr'
let g:rnvimr_layout = { 'relative': 'editor',
            \ 'width': float2nr(round(&columns)),
            \ 'height': float2nr(round(0.8 * &lines)),
            \ 'col': float2nr(round(0.5 * &columns)),
            \ 'row': float2nr(round(0.5 * &lines)),
            \ 'style': 'minimal' }
" "vim-floaterm{{{========================================================================
Plug 'voldikss/vim-floaterm', { 'on': 'FloatermToggle ' }
" let g:floaterm_width = &columns
" let g:floaterm_height = &lines
" let g:floaterm_winblend = 1
" "======================================================================================}}}

"Floaterm{{========================================
nmap <leader>o :FloatermToggle<cr>
"}}}==========================================
tnoremap O <C-\><C-n>:FloatermToggle<CR>

Plug 'voldikss/vim-floaterm'
let g:floaterm_width = &columns
let g:floaterm_height = &lines
let g:floaterm_winblend = 1

"tmuxline{{{==============================================================================
" Simple tmux statusline generator with support
" for powerline symbols and vim/airline/lightline
" statusline integration
Plug 'edkolev/tmuxline.vim'
"======================================================================================}}}
Perhaps
https://github.com/rottencandy/vimkubectl
https://github.com/segeljakt/vim-isotope
