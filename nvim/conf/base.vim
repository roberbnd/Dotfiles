"coc{{{===================================================================================
" Coc is an intellisense engine for vim8 & neovim.  It only works on vim >= 8.1 and neovim >= 0.3.1
" It's a completion framework, language server client which support extension features of VSCode
Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}

let g:coc_force_debug = 1
let g:coc_global_extensions = [
  \ 'coc-lists', 'coc-post', 'coc-lua', 'coc-word',
  \ 'coc-tailwindcss', 'coc-post', 'coc-project',
  \ 'coc-translator', 'coc-import-cost', 'coc-bookmark',
  \ 'coc-eslint', 'coc-prettier', 'coc-tsserver',
  \ 'coc-tslint', 'coc-tslint-plugin', 'coc-css',
  \ 'coc-json', 'coc-python', 'coc-yaml', 'coc-emmet',
  \ 'coc-pairs', 'coc-html', 'coc-highlight',
  \ 'coc-snippets', 'coc-solargraph', 'coc-rls',
  \ 'coc-marketplace', 'coc-git', 'coc-markmap',
  \ 'coc-gitignore', 'coc-github' ,'coc-yank',
  \ 'coc-jest', 'coc-go' ,'coc-docker', 'coc-lit-html',
  \ 'coc-markmap'
  \ ]
"======================================================================================}}}

"ranger{{{================================================================================
Plug 'rbgrouleff/bclose.vim'
Plug 'francoiscabrol/ranger.vim'
Plug 'kevinhwang91/rnvimr', {'do': 'make sync'}
let g:rnvimr_layout = { 'relative': 'editor',
            \ 'width': float2nr(round(&columns)),
            \ 'height': float2nr(round(0.8 * &lines)),
            \ 'col': float2nr(round(0.5 * &columns)),
            \ 'row': float2nr(round(0.5 * &lines)),
            \ 'style': 'minimal' }
"======================================================================================}}}

"easymotion{{{============================================================================
" EasyMotion provides a much simpler way to use some motions in vim
Plug 'easymotion/vim-easymotion'

let g:EasyMotion_smartcase = 1
let g:EasyMotion_use_smartsign_us = 1
let g:EasyMotion_do_mapping = 0
"======================================================================================}}}

"vista{{{=================================================================================
" Viewer & Finder for LSP symbols and tags in Vim required ctags installed
Plug 'liuchengxu/vista.vim', { 'on': 'Vista' }

" let g:vista_default_executive = 'ptags'
"======================================================================================}}}

"repeat{{{================================================================================
" If you've ever tried using the . command after a Plug map, you were
" likely disappointed to discover it only repeated the last native command
" inside that map, rather than the map as a whole. That disappointment ends today.
" Repeat.vim remaps . in a way that Plugs can tap into it.
Plug 'tpope/vim-repeat'
"======================================================================================}}}

"vim-surround{{{==========================================================================
" The Plug provides mappings to easily delete,
" change and add such surroundings in pairs. cs"'
" ysiw TAG
" ds"
" cst<h2<CR>
" Changes the tag but doesn't remove any attributes.
" cst<h2 style="font-size: larger"<CR>
" Changes the tag and adds a style attribute at the beginning.
" cst<h2><CR>
" Replace the tag, removes attributes.
" cst<h2 id="foo"><CR>
" Replaces the tag with attributes and all.
Plug 'tpope/vim-surround'
"======================================================================================}}}

"tcomment_vim{{{==========================================================================
" gcc
" <c-_>p Comment the current inner paragraph
Plug 'tomtom/tcomment_vim'
"======================================================================================}}}

"insertlessly{{{==========================================================================
" Enter and return in normal mode are working like insert mode
Plug 'dahu/insertlessly'

let g:insertlessly_insert_spaces = 0
let g:insertlessly_cleanup_trailing_ws = 0
let g:insertlessly_cleanup_all_ws = 0
"======================================================================================}}}

"vim-bookmarks{{{=========================================================================
" Add/remove bookmark at current line 	mm                : BookmarkToggle
" Add/edit/remove annotation at current line 	mi          : BookmarkAnnotate <TEXT>
" Jump to next bookmark in buffer 	mn                    : BookmarkNext
" Jump to previous bookmark in buffer 	mp                : BookmarkPrev
" Show all bookmarks (toggle) 	ma                        : BookmarkShowAll
" Clear bookmarks in current buffer only 	mc              : BookmarkClear
" Clear bookmarks in all buffers 	mx                      : BookmarkClearAll
" Move up bookmark at current line 	[count]mkk            : BookmarkMoveUp [<COUNT>]
" Move down bookmark at current line 	[count]mjj          : BookmarkMoveDown [<COUNT>]
" Move bookmark at current line to another line [count]mg : BookmarkMoveToLine <LINE>
" Save all bookmarks to a file                            : BookmarkSave <FILE_PATH>
" Load bookmarks from a file                              : BookmarkLoad <FILE_PATH>
Plug 'MattesGroeger/vim-bookmarks'

let g:bookmark_save_per_working_dir = 1
let g:bookmark_auto_save = 1
let g:bookmark_highlight_lines = 1
"======================================================================================}}}

"vim-stay{{{==============================================================================
" vim-stay adds automated view session creation and
" estoration whenever editing a buffer, across Vim
" sessions and window life cycles. It also alleviates
" Vim's tendency to lose view state when cycling through
" buffers (via argdo, bufdo et al.). It is smart about
" which buffers should be persisted and which should not,
" making the procedure painless and invisible.
Plug 'zhimsel/vim-stay'
"======================================================================================}}}

"vim-delete-hidden-buffers{{{=============================================================
" Delete all the buffers except the currrent
Plug 'arithran/vim-delete-hidden-buffers', { 'on': 'DeleteHiddenBuffers'}
"======================================================================================}}}

"vim-matchup{{{===========================================================================
" vim match-up: even better % fist_oncoming navigate and highlight matching words
" fist_oncoming modern matchit and matchparen replacement
" Plug 'andymass/vim-matchup'

let g:matchup_matchparen_enabled = 0
"======================================================================================}}}

"vim-indent-guides{{{=====================================================================
Plug 'nathanaelkane/vim-indent-guides', { 'on': 'IndentGuidesToggle' }

let g:indent_guides_auto_colors = 0
let g:indent_guides_guide_size = 1
"======================================================================================}}}

"vim-clap{{{==============================================================================
Plug 'liuchengxu/vim-clap'
 let g:clap_layout = { 'width': '100%', 'col': '5%' }
"======================================================================================}}}

"fzf{{{===================================================================================
" install bat
Plug 'yuki-ycino/fzf-preview.vim'
"======================================================================================}}}

"vim-quickui{{{===========================================================================
" menu like turboc
Plug 'skywind3000/vim-quickui'
"======================================================================================}}}

"vim-asterisk{{{==========================================================================
Plug 'haya14busa/vim-asterisk'
"======================================================================================}}}

"indentLine{{{==========================================================================
Plug 'Yggdroot/indentLine'
"======================================================================================}}}

"tmuxline{{{==============================================================================
" Simple tmux statusline generator with support
" for powerline symbols and vim/airline/lightline
" statusline integration
Plug 'edkolev/tmuxline.vim'
"======================================================================================}}}

"traces{{{================================================================================
" Preview in substitute
Plug 'markonm/traces.vim'
"======================================================================================}}}

"nerdtree{{{==============================================================================
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

let g:NERDTreeMapMenu = '+'
let g:NERDTreeWinSize = 25
let NERDTreeShowHidden=1
let g:Webdevicons_conceal_nerdtree_brackets = 0
let g:WebDevIconsNerdTreeAfterGlyphPadding = ' '
" Disable arrow icons at the left side of folders for NERDTree.
let g:NERDTreeDirArrowExpandable = "\u00a0"
let g:NERDTreeDirArrowCollapsible = "\u00a0"
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
"======================================================================================}}}
