"Description {===============================
" switching between a single-line statement and a multi-line one.
" It offers the following default keybindings, which can be customized:
" gS to split a one-liner into multiple lines
" gJ (with the cursor on the first line of a block)
"     to join a block into a single-line statement.
"     I usually work with ruby and a lot of expressions
"     can be written very concisely on a single line.
"  A good example is the "if" statement:
" puts "foo" if bar?
" This is a great feature of the language, but when you
" need to add more statements to the body of the "if", you need to rewrite it:
" if bar?
"   puts "foo"
"   puts "baz"
" end
" The idea of this plugin is to introduce a single key binding
" (default: gS) for transforming a line like this:
" <div id="foo">bar</div>
" Into this:
" <div id="foo">
"   bar
" </div>
"}===========================================
Plug 'AndrewRadev/splitjoin.vim'


"Description {===============================
" Let's assume that you have a project /home/user/work/my_project.
" This project have multiple environments - development, staging
" and production. Development - is your current local environment.
" Staging and production - remote environments, each placed on their
" own remote server. Project structure on each environments is nearly
" similar (from here comes the name of this plugin). If you want to
" get access to multiple environment-related remote actions, you
" need to add information about this project to configuration file.
" Run this command :MirrorConfig and edit configuration file.

" For our example it should look like this:

" /home/user/work/my_project:
"   staging: my_project@staging_host/current
"   production: my_project@production_host/current

" See Configuration for more details about format and structure of this file.
" From now, if you open any file inside /home/user/work/my_project
" then multiple remote commands should be available. For example,
" if you want to edit some file on remote server in staging
" environment (my_project@staging_host), then open this file
" locally and run :MirrorEdit staging. You should be able to
" edit this remote file here, locally, with your own vim settings.
" If you want to see difference between file, you currently edit
" and version of this file on production server - use this command:
" :MirrorDiff production.
"}===========================================
call dein#add('zenbro/mirror.vim')


"Description {===============================
" [- : Move to previous line of lesser indent than the current line.
" [+ : Move to previous line of greater indent than the current line.
" [= : Move to previous line of same indent as the current line 
" that is separated from the current line by lines of different indents.
" ]- : Move to next line of lesser indent than the current line.
" ]+ : Move to next line of greater indent than the current line.
" ]= : Move to next line of same indent as the current line 
" that is separated from the current line by lines of different indents.
"}===========================================
Plug 'jeetsukumaran/vim-indentwise'

"Description {===============================
":MdConvert,:MdSaveAs,:MdPreview,:MdStopPreview
"}===========================================
Plug 'kurocode25/mdforvim'

"Description {===============================
" Hyperfocus-writing in Vim.
"}===========================================
Plug 'junegunn/limelight.vim'

"Description {===============================
" When you perform a search, the cursor jumps 
" to the closest match. It's often hard to 
" locate it's new position. With this plugin 
" the cursor line (by default) or the search 
" pattern will "pulse" thus requiring your eyes attention.
"}===========================================
Plug 'inside/vim-search-pulse'

"Description {===============================
" It adds some useful mappings for manage simple todo lists
"}===========================================
Plug 'vitalk/vim-simple-todo'
Plug 'vim-scripts/hexHighlight.vim'
