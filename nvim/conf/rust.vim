"rust{{{==================================================================================
" This is a Vim plugin that provides Rust file
" detection, syntax highlighting, formatting,
" Syntastic integration, and more.
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
"======================================================================================}}}

"vim-racer{{{=============================================================================
" This plugin allows vim to use Racer for Rust code
" completion and navigation.
Plug 'racer-rust/vim-racer', { 'for': 'rust' }
let g:racer_cmd = "~/.cargo/bin/racer"
"======================================================================================}}}

"rust-cute-vim{{{=========================================================================
" Cute ligatures for Rust in Vim:
Plug 'ticki/rust-cute-vim', { 'for': 'rust' }
"======================================================================================}}}

"rustup{{{================================================================================
" A thin wrapper for calling rustup from Vim
Plug 'ubnt-intrepid/rustup.vim', { 'for': 'rust' }
"======================================================================================}}}
