colorscheme rigel
hi CursorLine guifg=reverse guibg=#03285C
hi Comment gui=italic guifg=#5C6370
hi Visual  guifg=#7eb2dd guibg=#00384d gui=none
hi EasyMotionTarget gui=bold guifg=#FFFF00
hi EasyMotionShade guifg=#607D8B
hi IndentGuidesOdd  guibg=#303030
hi IndentGuidesEven guibg=#5e605f

autocmd FileType python,html setlocal foldmethod=indent
autocmd FileType python setlocal tabstop=4 sts=4 sw=4

autocmd BufRead,BufNewFile *.sbt set filetype=scala
autocmd BufEnter *Spec.js,*_spec.js set filetype=jasmine
autocmd FileType html,css,scss,typescript,eruby,handlebars,javascript EmmetInstall
autocmd FileType javascript,css,less,sass,c,cpp setlocal foldmarker={,}

autocmd FileType ruby,html,css,scss,sass,typescript,snippets setlocal tabstop=2 sts=2 sw=2
autocmd FileType javascript,javascript.jsx,feature,yaml,vim,json,eruby setlocal tabstop=2 sts=2 sw=2

autocmd BufNewFile,BufReadPre *.babelrc set ft=json
autocmd BufNewFile,BufReadPre *.handlebars set ft=html
autocmd BufNewFile,BufReadPre *.hbs,*.ejs set ft=html
autocmd Filetype json let g:indentLine_setConceal = 0

call quickui#menu#install("&Git", [
			\ ['&Messager', 'GitMessenger'],
      \ ])

autocmd InsertEnter * inoremap ) <bs>
autocmd InsertEnter * inoremap ] <cr>
au VimEnter * startinsert
call feedkeys("\<esc>")
