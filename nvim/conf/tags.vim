"Description {===============================
" Tagbar is a vim plugin for browsing the tags
" of source code files. It provides a sidebar
" that displays the ctags-generated tags of the
" current file,ordered by their scope.
"}===========================================
Plug 'majutsushi/tagbar', { 'on': 'TagbarToggle'}

"Description {===============================
" Gutentags is a plugin that takes care of the
" much needed management of tags files in Vim.
"}===========================================
" Plug 'ludovicchabant/vim-gutentags'
