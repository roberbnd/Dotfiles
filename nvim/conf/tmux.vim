"vim-tmux-navigator{{{====================================================================
" This plugin provides the following mappings which allow
" you to move between Vim panes and tmux splits seamlessly.
"     <ctrl-h> => Left
"     <ctrl-j> => Down
"     <ctrl-k> => Up
"     <ctrl-l> => Right
"     <ctrl-\> => Previous split
Plug 'christoomey/vim-tmux-navigator'

if &term =~ '^screen'
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif
let g:tmux_navigator_no_mappings = 1
"======================================================================================}}}
