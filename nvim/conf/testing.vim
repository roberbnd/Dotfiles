"Description {===============================
" :TestNearest 	In a test file runs the test nearest to the cursor,
" otherwise runs the last nearest test. In test frameworks that don't
" support line numbers it will polyfill this functionality with regexes.
" :TestFile 	In a test file runs all tests in the current file, otherwise runs the last file tests.
" :TestSuite 	Runs the whole test suite (if the current file is a
" test file, runs that framework's test suite, otherwise determines
" the test framework from the last run test).
" :TestLast 	Runs the last test.
" :TestVisit 	Visits the test file from which you last run your
" tests (useful when you're trying to make a test pass, and you
" dive deep into application code and close your test buffer to
" make more space, and once you've made it pass you want to go
" back to the test file to write more tests).
"}===========================================
call dein#add('janko-m/vim-test')

" Description {==============================
" dispatch.vim: Asynchronous build and test dispatcher
"}===========================================
call dein#add('tpope/vim-dispatch')
call dein#add(radenling/vim-dispatch-neovim')

