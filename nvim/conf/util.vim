"loremipsum{{{============================================================================
" Command:
" :Loremipsum[!] [WORDCOUNT] [PARAGRAPH_TEMPLATE] [PREFIX POSTFIX]
"     Insert some random text.
" :Loreplace [REPLACEMENT] [PREFIX] [POSTFIX]
"     Replace the random text with something else or simply remove it.
Plug 'vim-scripts/loremipsum', { 'on': 'Loremipsum'}
"======================================================================================}}}

"vim-silicon{{{===========================================================================
" # Install cargo
" curl https://sh.rustup.rs -sSf | sh
" # Install silicon
" cargo install silicon
" This plugin provides a command which, given a
" visual selection or buffer, will generate a
" neat looking and highly customizable image
" of the source code.
Plug 'segeljakt/vim-silicon', { 'on': ['SiliconHighlight', 'Silicon']}
"======================================================================================}}}

"vim-doge{{{==============================================================================
" DoGe is a (Do)cumentation (Ge)nerator which will generate a proper documentation skeleton
" based on certain expressions (mainly functions)
Plug 'kkoomen/vim-doge', { 'on': 'DogeGenerate'}

let g:doge_enable_mappings = 0
"======================================================================================}}}
