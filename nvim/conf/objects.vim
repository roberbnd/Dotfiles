"vim-textobj-user{{{======================================================================
" vim-textobj-user is a Vim plugin to create
" your own text objects.
Plug 'kana/vim-textobj-user'
"======================================================================================}}}

"vim-textobj-entire{{{====================================================================
" vim-textobj-entire is a Vim plugin to provide
" text objects (ae and ie by default) to select
" the entire content of a buffer. Though these
" are trivial operations (e.g. ggVG), text object
" versions are more handy, because you do not have
" to be conscious of the cursor position (e.g. vae).
" vim-textobj-entire provides two text objects:
"
" ae targets the entire content of the current buffer.
" ie is similar to ae, but ie does not include leading and trailing empty lines.
Plug 'kana/vim-textobj-entire'
"======================================================================================}}}

"vim-textobj-punctuation{{{===============================================================
" vim-textobj-eunctuation is a Vim plugin providing text objects
" iu and au (u stands for pUnctuation, as p is already taken
" for paragraphs) for capturing the text between the current
" cursor position and a punctuation character in front. This
" punctuation character can be one of: ,, ., :, ;, !, and ?.
" The text object can span multiple lines to reach the closest
" punctuation character. Some examples (the cursor is shown with |):
"
" Before: String |foo = "bar";; after pressing diu: String |;
" Before: if |i == 42:; after pressing diu: if |:
" xmap u iu
" omap u iu
Plug 'beloglazov/vim-textobj-punctuation'

"vim-jasmine{{{===========================================================================
" Simple one operator-pending mapping r
" Operator-pending mapping r is added. dir, yar
" and other mappings are available like diw, yi'.
" if, unless, case, while, until, for, def, module,
" class, do, begin blocks are selected as text-objects.
Plug 'rhysd/vim-textobj-ruby', { 'for': 'ruby'}
Plug 'whatyouhide/vim-textobj-erb', { 'for': 'eruby'}
"======================================================================================}}}

"vim-jasmine{{{===========================================================================
"]]" : Move (forward) to the beginning of the next Python class.
"][" : Move (forward) to the end of the current Python class.
"[[" : Move (backward) to beginning of the current Python class
"(or beginning of the previous Python class if not currently
"in a class or already at the beginning of a class).
"[]" : Move (backward) to end of the previous Python class.
"]m" : Move (forward) to the beginning of the next Python method or function.
"]M" : Move (forward) to the end of the current Python method or function.
"[m" : Move (backward) to the beginning of the current Python
"method or function (or to the beginning of the previous method
"or function if not currently in a method/function or already
"at the beginning of a method/function).
"[M" : Move (backward) to the end of the previous Python
"method or function.
Plug 'jeetsukumaran/vim-pythonsense', { 'for': 'python'}
"======================================================================================}}}

"vim-jasmine{{{===========================================================================
" am: around method call. Gets the method call of the surrounding scope.
" im: inner method call. Gets the method call of the current scope.
" aM: around method call chain. Gets the method call chain of the surrounding scope.
" iM: inner method call chain. Gets the method call chain of the current scope.
Plug 'thalesmello/vim-textobj-methodcall', { 'for': 'javascript'}
"======================================================================================}}}

"vim-textobj-xmlattr{{{===================================================================
" This vim plugin provides two text objects: ax and ix.
" They represent XML/HTML attributes.
" It's based on (and requires) vim-textobj-user.
Plug 'whatyouhide/vim-textobj-xmlattr', { 'for': ['javascript', 'html', 'eruby'] }
"======================================================================================}}}

"dsf{{{===================================================================================
" By pressing dsf (which stands for "delete
" surrounding function call")
Plug 'AndrewRadev/dsf.vim'
"======================================================================================}}}
