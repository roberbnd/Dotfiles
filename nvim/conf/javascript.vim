"vim-jasmine{{{===========================================================================
" This is a syntax highlighting plugin for the Jasmine
Plug 'glanotte/vim-jasmine', { 'for': 'jasmine' }
"======================================================================================}}}

"vim-fixmyjs{{{===========================================================================
" auto fix your javascript using eslint
" or fixmyjs or jscs or tslint
Plug 'ruanyl/vim-fixmyjs', { 'for': 'javascript' }
let g:fixmyjs_use_local = 1
" let g:fixmyjs_rc_path = '~/Workspace/.eslintrc.js'
" let g:fixmyjs_rc_filename = ['.eslintrc.js', '.eslintrc.json']
"======================================================================================}}}

"vim-jsx-improve{{{=======================================================================
" syntax
Plug 'neoclide/vim-jsx-improve', { 'for': ['javascript.jsx', 'javascript'] }
let g:jsx_ext_required = 0 " Allow JSX in normal JS files
let g:vim_jsx_pretty_colorful_config = 1
"======================================================================================}}}

"vim-jest-test{{{=========================================================================
" RunJest - runs all the tests
" RunJestOnBuffer - runs all the tests in the current file
" RunJestFocused - runs the current test under the cursor
Plug 'tyewang/vimux-jest-test', { 'for': 'javascript' }
"======================================================================================}}}
