" ~/.config/nvim/sessions/elixir.vim:
" Vim session script.
" Created by session.vim 2.13.1 on 06 January 2018 at 17:04:58.
" Open this file in Vim and run :source % to restore your session.

if exists('g:syntax_on') != 1 | syntax on | endif
if exists('g:did_load_filetypes') != 1 | filetype on | endif
if exists('g:did_load_ftplugin') != 1 | filetype plugin on | endif
if exists('g:did_indent_on') != 1 | filetype indent on | endif
if &background != 'dark'
	set background=dark
endif
if !exists('g:colors_name') || g:colors_name != 'PaperColor' | colorscheme PaperColor | endif
call setqflist([])
let SessionLoad = 1
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
let NERDTreeMapPreviewSplit = "gi"
let NERDTreeShowHidden = "0"
let NERDTreeMinimalUI = "0"
let NERDUsePlaceHolders = "1"
let DirDiffSort =  1 
let WebDevIconsNerdTreeGitPluginForceVAlign =  1 
let NERDTreeRespectWildIgnore = "0"
let NERDTreeWinSize =  20 
let UltiSnipsRemoveSelectModeMappings =  1 
let NERDTreeMapCloseDir = "x"
let NERDTreeMapMenu = "m"
let NERDTreeMapJumpParent = "p"
let WebDevIconsUnicodeDecorateFolderNodesDefaultSymbol = ""
let NERDTreeShowBookmarks = "0"
let AutoPairsFlyMode =  0 
let NERDTreeMouseMode = "1"
let AutoPairsCenterLine =  1 
let NERDTreeShowGitStatus =  1 
let NERDTreeMapOpenSplit = "i"
let NERDTreeShowLineNumbers = "0"
let DirDiffForceLang = "C"
let NERDTreeHighlightCursorline = "1"
let UltiSnipsExpandTrigger = ";,"
let EasyMotion_use_migemo =  0 
let EasyMotion_show_prompt =  1 
let NERDCreateDefaultMappings =  0 
let NERDTreeMapPreviewVSplit = "gs"
let AutoPairsMultilineClose =  1 
let NERDTreeMapJumpRoot = "P"
let UltiSnipsSnippetsDir = "~/.config/nvim/UltiSnips"
let NERDTreeMapRefreshRoot = "R"
let NERDTreeCascadeOpenSingleChildDir = "1"
let DirDiffTextAnd = " and "
let NERDTreeMapOpenVSplit = "s"
let NERDTrimTrailingWhitespace = "0"
let NERDTreeMapDeleteBookmark = "D"
let NERDTreeMapJumpFirstChild = "K"
let DirDiffTextFiles = "Files "
let UltiSnipsEditSplit = "vertical"
let NERDTreeMapToggleBookmarks = "B"
let NERDTreeMapToggleHidden = "I"
let AutoPairsMapBS =  1 
let EasyMotion_move_highlight =  1 
let AutoPairsShortcutBackInsert = "<M-b>"
let AutoPairsLoaded =  1 
let EasyMotion_smartcase =  1 
let AutoPairsMapCR =  1 
let NERDTreeBookmarksFile = "/home/bnd/.NERDTreeBookmarks"
let UltiSnipsJumpForwardTrigger = "jj"
let AutoPairsMapCh =  1 
let Taboo_tabs = ""
let NERDTreeDirArrowExpandable = "▸"
let WebDevIconsUnicodeDecorateFileNodes =  1 
let NERDCommentEmptyLines = "0"
let EasyMotion_space_jump_first =  0 
let EasyMotion_prompt = "Search for {n} character(s): "
let EasyMotion_use_regexp =  1 
let NERDTreeMapOpenInTabSilent = "T"
let NERDTreeMapHelp = "?"
let NERDTreeMapJumpPrevSibling = "<C-k>"
let NERDDefaultNesting = "1"
let NERDTreeMapOpenInTab = "t"
let NERDTreeChDirMode = "0"
let NERDTreeAutoCenterThreshold = "3"
let NERDTreeShowFiles = "1"
let DirDiffIgnore = ""
let NERDTreeHijackNetrw = "1"
let NERDTreeBookmarksSort = "1"
let DirDiffLangString = "LANG=C "
let UltiSnipsJumpBackwardTrigger = "kk"
let DirDiffTextDiffer = " differ"
let EasyMotion_cursor_highlight =  1 
let CtrlSpaceStatuslineFunction = "airline#extensions#ctrlspace#statusline()"
let NERDBlockComIgnoreEmpty = "0"
let NERDTreeUpdateOnCursorHold =  1 
let NERDSpaceDelims = "0"
let DevIconsDefaultFolderOpenSymbol = ""
let NERDTreeMapToggleFiles = "F"
let NERDAllowAnyVisualDelims = "1"
let NERDTreeMapOpenRecursively = "O"
let NERDTreeMapUpdirKeepOpen = "U"
let EasyMotion_landing_highlight =  0 
let EasyMotion_off_screen_search =  1 
let DirDiffWindowSize =  14 
let NERDTreeSortHiddenFirst = "1"
let UltiSnipsEnableSnipMate =  1 
let NERDTreeAutoDeleteBuffer =  0 
let DevIconsEnableFoldersOpenClose =  0 
let EasyMotion_use_upper =  0 
let EasyMotion_do_mapping =  0 
let NERDTreeMapPreview = "go"
let NERDTreeCascadeSingleChildDir = "1"
let EasyMotion_disable_two_key_combo =  0 
let DevIconsEnableFolderPatternMatching =  1 
let NERDTreeShowIgnoredStatus =  0 
let NERDTreeAutoCenter = "1"
let AutoPairsShortcutFastWrap = "<M-e>"
let NERDMenuMode = "3"
let NERDTreeRemoveDirCmd = "rm -rf "
let CtrlSpaceLoaded =  1 
let EasyMotion_grouping =  1 
let WebDevIconsUnicodeDecorateFileNodesDefaultSymbol = ""
let EasyMotion_enter_jump_first =  0 
let EasyMotion_skipfoldedline =  1 
let NERDTreeCaseSensitiveSort = "0"
let NERDTreeMapPrevHunk = "[c"
let NERDTreeMapRefresh = "r"
let WebDevIconsUnicodeByteOrderMarkerDefaultSymbol = ""
let NERDDefaultAlign = "none"
let DirDiffExcludes = ""
let WebDevIconsUnicodeDecorateFolderNodes =  0 
let NERDTreeMapNextHunk = "]c"
let WebDevIconsUnicodeGlyphDoubleWidth =  1 
let WebDevIconsUnicodeDecorateFolderNodesExactMatches =  1 
let BufExplorer_title = "[Buf List]"
let NERDTreeCreatePrefix = "silent"
let AutoPairsShortcutJump = "<M-n>"
let AutoPairsMapSpace =  0 
let DirDiffTextOnlyIn = "Only in "
let EasyMotion_startofline =  1 
let EasyMotion_use_smartsign_us =  1 
let UltiSnipsListSnippets = "<c-tab>"
let Webdevicons_conceal_nerdtree_brackets =  0 
let NERDTreeMapChangeRoot = "C"
let NERDCompactSexyComs = "0"
let EasyMotion_keys = "asdghklqwertyuiopzxcvbnmfj;"
let DevIconsEnableFolderExtensionPatternMatching =  0 
let DirDiffIgnoreCase =  0 
let NERDTreeMapOpenExpl = "e"
let NERDTreeMapCloseChildren = "X"
let DirDiffEnableMappings =  0 
let AutoPairsShortcutToggle = "<M-p>"
let NERDTreeSortDirs = "1"
let WebDevIconsNerdTreeAfterGlyphPadding = ""
let NERDTreeNotificationThreshold = "100"
let NERDTreeMapActivateNode = "o"
let NERDTreeWinPos = "left"
let AutoPairsSmartQuotes =  1 
let NERDTreeStatusline = "%{exists('b:NERDTree')?b:NERDTree.root.path.str():''}"
let NERDTreeMapToggleFilters = "f"
let EasyMotion_add_search_history =  1 
let EasyMotion_do_shade =  1 
let DirDiffInteractive =  0 
let EasyMotion_inc_highlight =  1 
let NERDLPlace = "[>"
let NERDRemoveExtraSpaces = "0"
let NERDTreeMapCWD = "CD"
let DirDiffDynamicDiffText =  0 
let NERDTreeNaturalSort = "0"
let EasyMotion_verbose =  1 
let NERDTreeMapUpdir = "u"
let NERDCommentWholeLinesInVMode = "0"
let NERDTreeMapChdir = "cd"
let DirDiffTextOnlyInCenter = ": "
let NERDRPlace = "<]"
let NERDTreeMapToggleZoom = "A"
let NERDTreeMarkBookmarks = "1"
let NERDTreeUpdateOnWrite =  1 
let NERDRemoveAltComs = "1"
let NERDTreeMapJumpLastChild = "J"
let DirDiffAddArgs = ""
let NERDTreeMapJumpNextSibling = "<C-j>"
let NERDTreeCopyCmd = "cp -r "
let NERDTreeMapQuit = "q"
let EasyMotion_force_csapprox =  0 
let EasyMotion_loaded =  1 
let NERDTreeGlyphReadOnly = "RO"
let NERDTreeDirArrowCollapsible = "▾"
let AutoPairsMoveCharacter = "()[]{}\"'"
let NERDTreeQuitOnOpen = "0"
silent only
cd ~/Workspace/5Elixir/book
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +0 mm/export.exs
argglobal
silent! argdel *
$argadd mm/export.exs
edit mm/export.exs
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=1 winminwidth=1 winheight=1 winwidth=1
argglobal
setlocal fdm=marker
setlocal fde=0
setlocal fmr={,}
setlocal fdi=#
setlocal fdl=99
setlocal fml=1
setlocal fdn=20
setlocal fen
let s:l = 1 - ((0 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 05|
tabnext 1
if exists('s:wipebuf') && getbufvar(s:wipebuf, '&buftype') isnot# 'terminal'
"   silent exe 'bwipe ' . s:wipebuf
endif
" unlet! s:wipebuf
set winheight=1 winwidth=20 winminheight=1 winminwidth=1 shortmess=filnxtToO
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save

" Support for special windows like quick-fix and plug-in windows.
" Everything down here is generated by vim-session (not supported
" by :mksession out of the box).

1wincmd w
tabnext 1
if exists('s:wipebuf')
  if empty(bufname(s:wipebuf))
if !getbufvar(s:wipebuf, '&modified')
  let s:wipebuflines = getbufline(s:wipebuf, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:wipebuf
  endif
endif
  endif
endif
doautoall SessionLoadPost
unlet SessionLoad
" vim: ft=vim ro nowrap smc=128
